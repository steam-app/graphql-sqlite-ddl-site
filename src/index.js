import {parse, schemaHeader} from "@komino/graphql-sqlite-ddl"
import { buildSchema } from 'graphql'

export default {
  parse: parse,
  schemaHeader: schemaHeader,
  buildSchema: buildSchema,
}