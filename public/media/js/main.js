require(['./config'], () => {
  require(['lib'], (lib) => {
    const {parse, buildSchema, schemaHeader} = lib.default;

    const convert = () =>{
      const text = document.getElementById('input').value;
      const schema = buildSchema(schemaHeader + text);
      document.getElementById('output').value = parse(schema, false);
    }

    const convertORM = () =>{
      const text = document.getElementById('input').value;
      const schema = buildSchema(schemaHeader + text);
      document.getElementById('output').value = parse(schema);
    }

    const actionBtn = document.getElementById('action');
    actionBtn.addEventListener('click', convert);
    actionBtn.addEventListener('touchstart', convert);

    const actionBtn2 = document.getElementById('action2');
    actionBtn2.addEventListener('click', convertORM);
    actionBtn2.addEventListener('touchstart', convertORM);
  })
})